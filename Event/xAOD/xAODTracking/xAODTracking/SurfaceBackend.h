/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODTRACKING_SURFACEBACKEND_H
#define XAODTRACKING_SURFACEBACKEND_H

#include "xAODTracking/versions/SurfaceBackend_v1.h"

namespace xAOD {
  typedef SurfaceBackend_v1 SurfaceBackend;
}
#endif